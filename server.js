/*
*─────█─▄▀█──█▀▄─█───── 
*────▐▌──────────▐▌──── 
*────█▌▀▄──▄▄──▄▀▐█──── 
*───▐██──▀▀──▀▀──██▌─── 
*──▄████▄──▐▌──▄████▄──
*Writing by: Shinc0
*On: 20/12/2021
*If you have any question, feel free to contact me on discord: Shinco#1003
*/

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*                                                      MARK: VARIABLES                                                             */
/************************************************************************************************************************************/
/************************************************************************************************************************************/

const { Socket } = require('engine.io');
var express = require('express')
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const path = require('path');

let rooms = [];

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*                                                      MARK: APP                                                             */
/************************************************************************************************************************************/
/************************************************************************************************************************************/

http.listen(3000, () => {
    console.log('Server listening at port %d', 3000);
});

app.use(express.static(path.join(__dirname, 'www')));

io.on('connection', function(socket){
      
    console.log("\x1b[32m[Connection]\x1b[0m " + socket.id);

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*                                                      MARK: SOCKETS                                                               */
/************************************************************************************************************************************/
/************************************************************************************************************************************/

    socket.on('disconnect', function(){
        console.log("\x1b[31m[Disconnection]\x1b[0m " + socket.id);
        let room = null;

        rooms.forEach(r => {
            if (r.players.find(p => p.socketId == socket.id)) {
                console.log("\x1b[31m[Destruct Room]\x1b[0m " + r.id);
                room = r;
                op = room.players.find(p => p.socketId != socket.id);
                io.to(room.id).emit('win message', op);
                room.players.forEach(p => {
                    p.roomId = null;
                });
                rooms = rooms.filter(r => r.id != room.id);
            }
        });

    });

    socket.on('playerData', function(player){
        console.log("\x1b[32m[Player]\x1b[0m " + player.name);
        let room = null;

        if(!player.roomId){
            room = createRoom(player);
            console.log("\x1b[32m[Create Room]\x1b[0m " + room.id + " - " + player.name);
        } 
        else {
            room = rooms.find(r => r.id === player.roomId);
            room.players.push(player);
            console.log("\x1b[32m[Join Room]\x1b[0m " + room.id + " - " + player.name);
        }

        socket.join(room.id);
        io.to(socket.id).emit('join room', room.id);

        if(room.players.length == 2){
            io.to(room.id).emit('start game', room.players);
        }
    });

    socket.on('get rooms', () =>{
        io.to(socket.id).emit('list rooms', rooms);
    })
    socket.on('play', function(opponent){
        let room = null;
        console.log("\x1b[32m[Play]\x1b[0m " + opponent.name + " - " +opponent.playedCell);
        
        room = rooms.find(r => r.players.find(p => p.socketId == opponent.socketId));
        io.to(room.id).emit('played cell', opponent);

    });

    socket.on('win', function(player){
        console.log("\x1b[32m[Win]\x1b[0m " + player.name);
        let room = null;
        room = rooms.find(r => r.players.find(p => p.socketId == player.socketId));
        io.to(room.id).emit('win message', player);
    });

    socket.on('equality', function(player){
        let room = null;
        room = rooms.find(r => r.players.find(p => p.socketId == player.socketId));
        io.to(room.id).emit('equality message', player);
    });
});

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*                                                      MARK: FUNCTIONS                                                             */
/************************************************************************************************************************************/
/************************************************************************************************************************************/

/*Create a new room*/
function createRoom(player){
    const room = {id : roomId(), players: []};
    player.roomId = room.id;
    room.players.push(player);
    rooms.push(room);
    return room;
}

/*Generate a new room id*/
function roomId(){
    return Math.random().toString(36).substring(2, 9);
}
