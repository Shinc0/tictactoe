/*
*─────█─▄▀█──█▀▄─█───── 
*────▐▌──────────▐▌──── 
*────█▌▀▄──▄▄──▄▀▐█──── 
*───▐██──▀▀──▀▀──██▌─── 
*──▄████▄──▐▌──▄████▄──
*Writing by: Shinc0
*On: 20/12/2021
*If you have any question, feel free to contact me on discord: Shinco#1003
*/

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*                                                      MARK: VARIABLES                                                             */
/************************************************************************************************************************************/
/************************************************************************************************************************************/

var socket = io();

var opponent = null;

const player = {
    name: "",
    socketId: "",
    playedCell:"",
    roomId: null,
    symbol: "0",
    turn: false,
    win: false
}

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*                                                      MARK: SOCKETS                                                               */
/************************************************************************************************************************************/
/************************************************************************************************************************************/

window.onload = function() {

    /*Affiche le menu principale */
    document.getElementById("container-board").style.display = "none";
    document.getElementById("firstpage").style.display = "block";
    document.getElementById("menu").style.display = "block";
    document.getElementById("waiting").style.display = "none";    

    /*Sélectionne tout les boutons du plateau de jeu et ajoute un évenement lors d'un clic sur l'un d'eux */
    var divs = document.querySelectorAll('div.cell'); 
    for (i = 0; i < divs.length; ++i) {
        divs[i].addEventListener('click', function(e) {
            if(e.target.innerHTML == "" && player.turn){
                e.target.innerHTML = player.symbol;  //Afficher le symbole du joueur
                player.playedCell = e.target.id;    //Ajouter l'id de la case jouée au joueur
                verifyGrid();                      //Vérifier si le joueur a gagné
                socket.emit('play', player);      //Retourner que le joueur a joué 
                
            }
        });
    };
}
/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*                                                      MARK: SOCKETS                                                               */
/************************************************************************************************************************************/
/************************************************************************************************************************************/

/*Quand le jeu démare */
socket.on('start game', function(players) {
    document.title =  players[0].name + " vs " + players[1].name;       //Changer le titre de la page avec le nom des joueurs
    document.getElementById("container-board").style.display = "flex"; //Afficher le plateau de jeu
    document.getElementById("firstpage").style.display = "none";      //Cacher le menu

    opponent = players.find(p => p.socketId != socket.id);          //Récupèrer l'adversaire du joueur

    /*Affiche le tour du joueur ou de l'adversaire*/
    if (player.turn) {
        document.getElementById("turn").innerHTML = "Your turn";
    } else {
        document.getElementById("turn").innerHTML = opponent.name +"'s turn";
    }
});

/*Quand le joueur a placé un symbole*/
socket.on('played cell', function(p) {
    
    /*Si le joueur n'est pas le joueur courant*/
    if(p.socketId != player.socketId){
        document.getElementById(p.playedCell).innerHTML = p.symbol; //Afficher le symbole de l'adversaire
        document.getElementById("turn").innerHTML = "Your turn";   //Afficher le tour du joueur
        player.turn = true;                                       //Le joueur devient le joueur courant 
        opponent.turn = false;                                   //L'adversaire n'est plus le joueur courant

    }
    else{
        document.getElementById("turn").innerHTML = opponent.name +"'s turn"; //Afficher le tour de l'adversaire
        player.turn = false;                                                 //Le joueur n'est plus le joueur courant
        opponent.turn = true;                                               //L'adversaire devient le joueur courant
        
    }
    
});

/*Quand l'un des joueurs a gagné*/
socket.on('win message', function(p) {
    if(p.socketId != player.socketId){ //Si le joueur n'est pas celui qui a gagné
        /*Afficher le message de défaite*/
        document.getElementById("overlay1").style.display = "none";   //Ne pas jouer l'animation de victoire
        document.getElementById("overlay2").style.display = "block"; //Afficher l'overlay de fin de partie
        document.getElementById("text").innerHTML = "You've lose!"; //Afficher le message de défaite
    }
    else{
        document.getElementById("overlay1").style.display = "block"; //Jouer l'annimation   
        document.getElementById("overlay2").style.display = "block";//Afficher l'overlay de fin de partie
        document.getElementById("text").innerHTML = "You've win!"; //Afficher le message de victoire
    }
    resetGame(); //Remise à zéro du plateau de jeux
});

/*Quand il y a égalité*/
socket.on('equality message', function(p) {
        document.getElementById("overlay1").style.display = "none";  //Ne pas jouer l'animation de victoire
        document.getElementById("overlay2").style.display = "block";//Afficher l'overlay de fin de partie
        document.getElementById("text").innerHTML = "Equality!";   //Afficher le message d'égalité
});

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*                                                      MARK: FUNCTIONS                                                             */
/************************************************************************************************************************************/
/************************************************************************************************************************************/

function startOnline() {
    /*Cache les overlays */
    document.getElementById("overlay1").style.display = "none";
    document.getElementById("overlay2").style.display = "none";

    player.name = document.getElementById("name").value; //Récupèrer le nom du joueur
    player.socketId = socket.id;                        //Ajouter le socketId au joueur

    /*Match Making: récuperer la liste des rooms ajoute ou créer une room*/
    socket.emit('get rooms'); 
    socket.on('list rooms', function(rooms) {
        if (rooms.length == 0) {                 //Si la liste est vide, créer une room
            player.turn = true;                 //Le joueur est le premier à jouer
            socket.emit('playerData', player); //Envoie les données du joueur au serveur
        } 
        else {
            rooms.forEach(room => {                         //Sinon, parcourir la liste des rooms
                if (room.players.length < 2) {             //Si la room n'a pas 2 joueurs
                    player.turn = false;                  //Le joueur n'est pas le premier à jouer
                    player.symbol = "j";                 //Le symbole du joueur est "j" soit "O"
                    player.roomId = room.id;            //Ajouter l'id de la room au joueur
                    socket.emit('playerData', player); //Envoie les données du joueur au serveur
                    return;
                }
            });
            if(player.roomId == null){               //Si le joueur n'a pas trouvé de room
                player.turn = true;                 //Le joueur est le premier à jouer
                socket.emit('playerData', player); //Envoie les données du joueur au serveur et créer une room
            }
        }
    });

    /*Affiche le menu d'attente de joueur*/
    document.getElementById("container-board").style.display = "none";
    document.getElementById("firstpage").style.display = "block";
    document.getElementById("menu").style.display = "none";
    document.getElementById("waiting").style.display = "block";
}

function verifyGrid(){
    var divs = document.querySelectorAll('div.cell'); 
    var grid = [];
    for (i = 0; i < divs.length; ++i) {
        grid.push(divs[i].innerHTML);
    }

    //equality
    var j = 0;
    for(i = 0; i < grid.length; i++){
        if(grid[i] != ""){
            j++;
        }
    }
    if(j == 9){
        socket.emit('equality', player);
    }

    //Columns
    for(i=0; i<3; i++){
        if(grid[i] == grid[i+3] && grid[i] == grid[i+6]){
            if(grid[i] == player.symbol){
                player.win = true;
                socket.emit('win', player);
            }
        }
    }

    //Rows
    for(i=0; i<9; i+=3){
        if(grid[i] == grid[i+1] && grid[i] == grid[i+2]){
            if(grid[i] == player.symbol){
                player.win = true;
                socket.emit('win', player);
            }
        }
    }

    //Diagonal
    if(grid[0] == grid[4] && grid[0] == grid[8]){
        if(grid[0] == player.symbol){
            player.win = true;
            socket.emit('win', player);
        }
    }

    //Anti-Diagonal
    if(grid[2] == grid[4] && grid[2] == grid[6]){
        if(grid[2] == player.symbol){
            player.win = true;
            socket.emit('win', player);
        }
    }
}

function resetGame(){

    var divs = document.querySelectorAll('div.cell'); 
    for (i = 0; i < divs.length; ++i) {
        divs[i].innerHTML = "";
    }
    player.win = false;
    player.turn = false;
    player.playedCell = "";
    player.roomId = null;
}


