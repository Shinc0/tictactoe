
# TicTacToe Game/Server

L'objectif de ce projet est de créer un serveur et une interface utilisateur pour le jeux du morpion (TicTacToe).



## Deployment

Ce projet utilise la technologie node.js.

Dans un premier temps il est nécessaire de copier le repos dans votre répertoire.
```
  git clone https://gitlab.com/Shinc0/tictactoe
  cd tictactoe
```
Il vous faut ensuite ajouter quelques librairies :
  - Express
  - Socket.io

```
  npm install --save express@4.15.2
  npm install --save socket.io
```

Et pour lancer le projet il vous suffira juste d'exécuter 

```
  node server.js
```

## Color Reference
- ![#17a8be](https://via.placeholder.com/15/17a8be/000000?text=+) `#17a8be`
- ![#FED766](https://via.placeholder.com/15/FED766/000000?text=+) `#FED766`
- ![#1589F0](https://via.placeholder.com/15/ecf0f3/000000?text=+) `#ecf0f3`


## Authors

- [@Shinc0](https://gitlab.com/Shinc0)


## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://www.shinco.tech/)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://fr.linkedin.com/in/romain-pipon)


